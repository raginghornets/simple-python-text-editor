import tkinter as tk
import tkinter.filedialog

root = tk.Tk("Text Editor")

text = tk.Text(root)
text.grid()

def saveas():
    global text
    t = text.get("1.0", "end-1c")
    saveLocation = tk.filedialog.asksaveasfilename()
    file1 = open(saveLocation, "w+")
    file1.write(t)
    file1.close()

button = tk.Button(root, text="Save", command=saveas)
button.grid()

def FontHelvetica():
    global text
    text.config(font = "Helvetica")

def FontCourier():
    global text
    text.config(font = "Courier")

font = tk.Menubutton(root, text = "Font")
font.grid()
font.menu = tk.Menu(font, tearoff = 0)
font["menu"] = font.menu
helvetica = tk.IntVar()
courier = tk.IntVar()
font.menu.add_checkbutton(label = "Courier", variable = courier, command = FontCourier)
font.menu.add_checkbutton(label = "Helvetica", variable = helvetica, command = FontHelvetica)
    
root.mainloop()
